<?php
/**
 * Intrepid-Web.NET
 * 
 * Source code is free to use for anyone, however please leave a credit 
 * to Intrepid-Web.NET if and when using any part of this code is used.
 * 
 * @copyright   Copyright (c) 2010 IntrepidWeb. (www.intrepid-web.net)
 */

# Configure
$dbFile = 'db/ItemsDB.db';
$sqlTableFile = 'inc/Items.sql';
$sqlFilesDir = 'sql/';

$sqlRegEx = "/INSERT INTO .* \(item_id\,item_code\,item_name\) VALUES \(\'(\d+)\'\,\'([a-zA-Z0-9]+)\'\,\'(.*)'\)\;/";