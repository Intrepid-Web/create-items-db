CREATE TABLE Items (
  id INTEGER PRIMARY KEY NOT NULL,
  kind INTEGER NOT NULL,
  item_id INTEGER NOT NULL,
  item_code VARCHAR(8) NOT NULL,
  item_name VARCHAR(255) NOT NULL
 );
CREATE INDEX idx_Items_ItemID ON Items (item_id,item_code);