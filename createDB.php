#!/usr/bin/env php
<?php
/**
 * Intrepid-Web.NET
 * 
 * Source code is free to use for anyone, however please leave a credit 
 * to Intrepid-Web.NET if and when using any part of this code is used.
 * 
 * @copyright   Copyright (c) 2010 IntrepidWeb. (www.intrepid-web.net)
 */

# CLI Mode only
if(php_sapi_name() !== 'cli') {
	print "<br/>Command line usage only";
	exit;
}

set_time_limit(0); 

# Includes
error_reporting(E_ALL);
require_once "inc/config.php";
require_once "inc/functions.php";
require_once "inc/cli_colors.php";
require_once "inc/ProgressBar.php";

$color = new Colors();

// Clear screen before we begin
clearscreen();
print "\n";

// New database per-session
@unlink($dbFile);
if(file_exists($dbFile)) {
	$color->failMessage("ItemsDB already exists. Delete the existing one to create a new one. \n");
	exit(1);	
}

// SQLite DB Connectivity
if ($sqliteDb = new PDO('sqlite:'.$dbFile)) { 
    $sqliteDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // Database successfully created
    $color->successMessage("Database created \n");
    
    // Create the table
    $sqlDump = file_get_contents($sqlTableFile);
    try {
        $sqliteDb->exec($sqlDump);
    } catch (PDOException $e) {
        die("Error! ".$e->getMessage());
    }
    $color->successMessage("Items table successfully created \n");
    $color->successMessage("Adding the items to the database...please wait... \n");
    
 	// The following is going to occur now:
 	// 1. Get all the SQL files for each item type
 	// 2. Parse each sql file so that we convert it into a single-table structure format
 	// 3. Do bulk-inserts per-sql file
    if ($handle = opendir($sqlFilesDir)) {
    	while (false !== ($file = readdir($handle))) {
    	
    		// We want to do a bulk insert, helps speed things up!
    		$sqliteDb->beginTransaction();
    		
    		if ($file != "." && $file != "..") {
    			// We need our itemKindIndex, to store in our database
				$new_name = $file;
				$new_name = strtolower($new_name);
				$new_name = str_replace("_str_dat","",$new_name);
				$new_name = preg_replace("/item.sql/","",$new_name);
				$new_name = str_replace(".sql", "", $new_name);
				$table_name = "tbl_code_".$new_name;
    			$itemKindIndex = getItemKindIndexByTableName($table_name);
    			
    			if($itemKindIndex === false) {
    				$color->failMessage("Woops, invalid item table provided: $table_name > $itemKindIndex\n");
    				$sqliteDb->rollBack();
    				exit(1);
    			}
    			
    			// Now we need to take all the information, phrase it so that it works to our liking :)
    			$sqlContents = file_get_contents($sqlFilesDir.$file);
    			
    			if(preg_match_all($sqlRegEx, $sqlContents, $matches)) {
    				$totalItems = count($matches[1]);
    				for($i=0; $i<$totalItems; ++$i) {
    					$index = $matches[1][$i];
    					$code = $matches[2][$i];
    					$name = $matches[3][$i];
    					
    					$sql = "INSERT INTO Items (kind, item_id, item_code, item_name) VALUES (".$itemKindIndex.", '$index', '$code', '$name')";
    				   try {
                    		$sqliteDb->exec($sql);
                		} catch ( PDOException $e) {
                    		$color->failMessage("Could not add item, SQL ERROR\n".$e->getMessage()."\n\n".$sql);
                    		$sqliteDb->rollBack();
                    		exit(1);
                		}
    				}
    			} else {
    				$color->failMessage("Regular expression seems to have failed. Please check it! \n");
    				$sqliteDb->rollBack();
    				exit(1);
    			}
    			
    		}
    		
    		// Commit our data
    		$sqliteDb->commit();
    		
    	}
    }
        
    $color->successMessage("Items Have been loaded \n");
} else {
    $color->failMessage($sqliteerror);
}

// EOF
print "\n";
exit(0);